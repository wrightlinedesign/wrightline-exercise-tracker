import { newE2EPage } from '@stencil/core/testing';

describe('app-message', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<app-message></app-message>');

    const element = await page.find('app-message');
    expect(element).toHaveClass('hydrated');
  });
});
