import { convertToAttributes } from '../../utils/storybookUtils';

export default {
  title: 'Components/App Message',
  component: 'app-message',
  argTypes: {
    type: {
      control: {
        disable: true,
        type: 'select',
        options: [
          'success',
          'warning',
          'error'
        ],
      }
    }
  }
}

const Template = (args:StorybookArgs) => `<app-message class="active" ${convertToAttributes(args).join(' ')}></app-message>`;

export const Default = Template.bind({});
Default.args = { message: 'Message' }

export const Success = Template.bind({});
Success.args = {
  type: 'success',
  message: 'Success Message'
};

export const Warning = Template.bind({});
Warning.args = {
  type: 'warning',
  message: 'Warning Message'
}

export const Error = Template.bind({});
Error.args = {
  type: 'error',
  message: 'Error Message'
};

export const All = Template.bind({});
All.args = { message: 'Everything is customisable' }
All.argTypes = {
  type: {
    control: {
      disable: false
    }
  }
}
