import { Component, Host, h, Prop } from '@stencil/core';
import { RouterHistory } from '@stencil/router';
import FirebaseAuthService from '../../services/auth.service';
import setAppMessage from '../../utils/setAppMessage';

@Component({
  tag: 'logout-button',
  styleUrl: 'logout-button.css',
  shadow: false,
})
export class LogoutButton {
  @Prop() history: RouterHistory;
  @Prop() text: string;

  async handleClick() {
    const logoutResponse = await FirebaseAuthService.logOut();
    if (logoutResponse.status === 200) {
      setAppMessage('success', logoutResponse.message);
    }
  }

  render() {
    return (
      <Host>
        <button onClick={this.handleClick}>{this.text}</button>
      </Host>
    );
  }

}
