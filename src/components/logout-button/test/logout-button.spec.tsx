import { newSpecPage } from '@stencil/core/testing';
import { LogoutButton } from '../logout-button';

describe('logout-button', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [LogoutButton],
      html: `<logout-button text="test text"></logout-button>`,
    });
    expect(page.root).toEqualHtml(`
      <logout-button text="test text">
          <button>test text</button>
      </logout-button>
    `);
  });
});
