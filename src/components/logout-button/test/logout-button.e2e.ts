import { newE2EPage } from '@stencil/core/testing';

describe('logout-button', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<logout-button></logout-button>');

    const element = await page.find('logout-button');
    expect(element).toHaveClass('hydrated');
  });
});
