import { Component, Host, h, Prop } from '@stencil/core';
import { RouterHistory, MatchResults } from '@stencil/router';
import state from '../../store';

@Component({
  tag: 'login-page',
  styleUrl: 'login-page.css',
  shadow: false,
})
export class LoginPage {
  @Prop() history: RouterHistory;
  @Prop() match: MatchResults;

  render() {
    if(state.isAuthenticated) {
      const { previousUrl } = state;
      if (previousUrl) {
        return <stencil-router-redirect url={previousUrl} />
      }
      return <stencil-router-redirect url="/dashboard" />
    }
    return (
      <Host>
        <div class="login-page-forms">
          <login-form></login-form>
          <register-form></register-form>
        </div>
        <div class="login-page-img">
          <img src="/assets/img/login-back.jpg" alt="dumbbells"/>
        </div>
      </Host>
    );
  }
}
