import { newSpecPage } from '@stencil/core/testing';
import { AddWorkout } from '../add-workout';

describe('add-workout', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [AddWorkout],
      html: `<add-workout></add-workout>`,
    });
    expect(page.root).toEqualHtml(`
      <add-workout>
        <button class="add-workout-button">+ Add a new workout</button>
        <wl-modal>
          <h2 slot="modal-title">Add a workout</h2>
          <div slot="modal-content">
            <form action="">
              <label htmlfor="workout-name">
                <input type="text" name="workout-name" id="workout-name">
              </label>
              <button type="submit">Add</button>
            </form>
          </div>
        </wl-modal>
      </add-workout>
    `);
  });
});
