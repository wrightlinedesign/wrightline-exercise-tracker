import { newE2EPage } from '@stencil/core/testing';

describe('add-workout', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<add-workout></add-workout>');

    const element = await page.find('add-workout');
    expect(element).toHaveClass('hydrated');
  });
});
