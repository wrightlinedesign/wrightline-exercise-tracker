import { Component, Host, h, Prop, Listen } from '@stencil/core';
import { RouterHistory, injectHistory } from '@stencil/router';
import { db } from '../../services/firebase';
import state from '../../store';
import moment from 'moment';
import setAppMessage from '../../utils/setAppMessage';

@Component({
  tag: 'add-workout',
  styleUrl: 'add-workout.css',
  shadow: false,
})
export class AddWorkout {
  @Prop() history: RouterHistory;
  @Prop() modalStatus: boolean;
  @Prop() workoutName: string;

  @Listen('wlModalClosed')
  wlModalClosedHandler() {
    this.modalStatus = false;
  }

  async addWorkout(event: any) {
    event.preventDefault();

    const workoutName = this.workoutName;
    if (!workoutName) {
      setAppMessage('error', 'Please add a workout name');
      return;
    }
    const response = await db.ref(`workouts/${state.uid}`).push({
      name: workoutName,
      dateAdded: moment().format('DD/MM/YYYY HH:mm')
    });
    this.workoutName = '';
    this.modalStatus = false;
    const workoutId = response.key;
    this.history.push(`/workout/${workoutId}`);
  }

  openModal() {
    this.modalStatus = true;
  }

  render() {
    return (
      <Host>
        <button class="add-workout-button" onClick={() => this.openModal()}>+ Add a new workout</button>
        <wl-modal shouldOpen={this.modalStatus}>
          <h2 slot="modal-title">Add a workout</h2>
          <div slot="modal-content">
            <form action="" onSubmit={(event) => this.addWorkout(event)}>
              <label htmlFor="workout-name">
                <input
                  type="text"
                  name="workout-name"
                  id="workout-name"
                  onInput={(event: any) =>  this.workoutName = event.target.value}
                />
              </label>
              <button type="submit">Add</button>
            </form>
          </div>
        </wl-modal>
      </Host>
    );
  }

}
injectHistory(AddWorkout);
