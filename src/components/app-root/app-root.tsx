import { Component, h, Host } from '@stencil/core';
import PrivateRoute from '../PrivateRoute';
import state from '../../store';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css',
  shadow: false,
})
export class AppRoot {
  render() {
    return (
      <Host>
        {state.isAuthenticated &&
          <header>
            <stencil-route-link url="/dashboard">
              <h1>Wrightline Exercise Tracker</h1>
            </stencil-route-link>
            <logout-button text="logout"></logout-button>
          </header>
        }
        <main>
          <stencil-router historyType="hash">
            <stencil-route-switch scrollTopOffset={0}>
              <stencil-route url="/" component="login-page" exact={true} />
              <stencil-route url="/upcoming-features" component="upcoming-features" exact={true} />
              <PrivateRoute url="/dashboard" routeRender={(props: any) =>
                <main-dashboard {...props}></main-dashboard>
              } />
              <PrivateRoute url="/workout/:id" routeRender={(props: any) =>
                <workout-page {...props}></workout-page>
              } />
            </stencil-route-switch>
          </stencil-router>
          <app-message type={state.appMessage.type} message={state.appMessage.messageText}></app-message>
        </main>
      </Host>
    );
  }
}
