import { Component, Host, h, Prop, State } from '@stencil/core';
import { db } from '../../services/firebase';
import state from '../../store';

@Component({
  tag: 'exercise-detail',
  styleUrl: 'exercise-detail.css',
  shadow: false,
})
export class ExerciseDetail {
  @Prop() exerciseId!: string;
  @State() exercise: IExercise;

  async componentWillLoad() {
    if(this.exerciseId) {
      const exerciseRef = await db.ref(`exercises/${state.uid}/${this.exerciseId}`).once('value');
      this.exercise = exerciseRef.val();
    }
  }

  render() {
    return (
      <Host>
        <h3>{this.exercise.name}</h3>
        <set-information exerciseId={this.exerciseId}></set-information>
        <exercise-detail-form exerciseId={this.exerciseId}></exercise-detail-form>
      </Host>
    );
  }

}
