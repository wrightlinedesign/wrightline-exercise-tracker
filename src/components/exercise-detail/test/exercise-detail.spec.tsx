import { newSpecPage } from '@stencil/core/testing';
import { ExerciseDetail } from '../exercise-detail';

// TODO: Decide whether spec testing is needed

describe.skip('exercise-detail', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [ExerciseDetail],
      html: `<exercise-detail></exercise-detail>`,
    });
    expect(page.root).toEqualHtml(`
      <exercise-detail>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </exercise-detail>
    `);
  });
});
