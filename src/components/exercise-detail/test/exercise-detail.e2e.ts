import { newE2EPage } from '@stencil/core/testing';

describe('exercise-detail', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<exercise-detail></exercise-detail>');

    const element = await page.find('exercise-detail');
    expect(element).toHaveClass('hydrated');
  });
});
