import { Component, Host, h, State, Prop } from '@stencil/core';
import { RouterHistory, injectHistory } from '@stencil/router';
import FirebaseAuthService from '../../services/auth.service';
import setAppMessage from '../../utils/setAppMessage';

@Component({
  tag: 'login-form',
  styleUrl: 'login-form.css',
  shadow: false,
})
export class LoginForm {
  @Prop() history: RouterHistory;
  @State() email: string;
  @State() password: string;

  async handleLogin(event) {
    event.preventDefault();
    const loginRes = await FirebaseAuthService.login({ email: this.email, password: this.password });
    if (loginRes.status == 200) {
      // reset inputs
      this.email= '';
      this.password = '';

      // move a user on
      this.history.push('/dashboard');
      setAppMessage('success', loginRes.message);
    } else {
      // reset inputs
      this.email= '';
      this.password = '';
      setAppMessage('error', 'Something went wrong please try again');
    }
  }

  render() {
    return (
      <Host>
        <h2>Login</h2>
        <form onSubmit={e => this.handleLogin(e)} novalidate>
          <custom-input
            labelText="Email"
            type="text"
            name="email"
            inputId="email"
            value={this.email}
            onInput={(event: any) =>  this.email = event.target.value}
          />
          <custom-input
            labelText="Password"
            type="password"
            name="password"
            inputId="password"
            value={this.password}
            onInput={(event: any) =>  this.password = event.target.value}
          />
          <button type="submit">Login</button>
        </form>
      </Host>
    );
  }

}
injectHistory(LoginForm);
