import { newSpecPage } from '@stencil/core/testing';
import { LoginForm } from '../login-form';

describe('login-form', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [LoginForm],
      html: `<login-form></login-form>`,
    });
    expect(page.root).toEqualHtml(`
      <login-form>
          <h2>Login</h2>
          <form novalidate="">
            <custom-input
              inputid="email"
              labeltext="Email"
              name="email"
              type="text">
            </custom-input>
            <custom-input
              inputid="password"
              labeltext="Password"
              name="password"
              type="password">
            </custom-input>
            <button type="submit">Login</button>
          </form>
      </login-form>
    `);
  });
});
