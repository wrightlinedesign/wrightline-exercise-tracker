import { Component, Host, h, Prop, State } from '@stencil/core';
import { MatchResults, RouterHistory, injectHistory } from '@stencil/router';
import { db } from '../../services/firebase';
import state from '../../store';
import setAppMessage from '../../utils/setAppMessage';

@Component({
  tag: 'workout-page',
  styleUrl: 'workout-page.css',
  shadow: false,
})
export class WorkoutPage {
  @State() workout: IWorkout;

  @Prop() match: MatchResults;
  @Prop() history: RouterHistory;

  componentWillLoad() {
    if (!this.match || !this.match.params.id) {
      setAppMessage('error', 'No ID was passed');
      this.history.push('/dashboard');
      return;
    }

    const workoutRef = db.ref(`workouts/${state.uid}/${this.match.params.id}`);
    workoutRef.on('value', (snapshot) => {
      const workout = snapshot.val();
      if (workout) {
        this.workout = workout;
      } else {
        setAppMessage('error', 'Sorry, it seems that workout doesn\'t exist');
        this.history.push('/dashboard');
        return;
      }
    })
  }

  render() {
    if (!this.workout) {
      return(
        <Host>
          <skeleton-text-item width="100" />
          <skeleton-text-item width="80" />
          <skeleton-card>
            <skeleton-text-item width="80" />
            <skeleton-text-item width="50" />
            <skeleton-text-item width="50" />
            <skeleton-text-item width="50" />
          </skeleton-card>
        </Host>
      );
    }
    const { name, dateAdded } = this.workout;
    return (
      <Host>
        <h2>{name}</h2>
        <span>{dateAdded}</span>
        <exercise-list workout={this.match.params.id}></exercise-list>
        <add-exercise workout={this.match.params.id}></add-exercise>
      </Host>
    );
  }

}
injectHistory(WorkoutPage);
