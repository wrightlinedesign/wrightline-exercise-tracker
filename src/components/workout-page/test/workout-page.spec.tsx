import { newSpecPage } from '@stencil/core/testing';
import { WorkoutPage } from '../workout-page';

describe.skip('workout-page', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [WorkoutPage],
      html: `<workout-page></workout-page>`,
    });
    expect(page.root).toEqualHtml(`
      <workout-page>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </workout-page>
    `);
  });
});
