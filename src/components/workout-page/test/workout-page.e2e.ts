import { newE2EPage } from '@stencil/core/testing';

describe('workout-page', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<workout-page></workout-page>');

    const element = await page.find('workout-page');
    expect(element).toHaveClass('hydrated');
  });
});
