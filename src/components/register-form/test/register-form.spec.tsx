import { newSpecPage } from '@stencil/core/testing';
import { RegisterForm } from '../register-form';

describe('register-form', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [RegisterForm],
      html: `<register-form></register-form>`,
    });
    expect(page.root).toEqualHtml(`
      <register-form>
          <h2>Register</h2>
          <form novalidate="">
              <custom-input
                inputid="register-name"
                labeltext="Display Name"
                name="register-name"
                type="text">
              </custom-input>
              <custom-input
                inputid="register-email"
                labeltext="Email"
                name="register-email"
                type="text">
              </custom-input>
              <custom-input
                inputid="register-password"
                labeltext="Password"
                name="register-password"
                type="password">
              </custom-input>
            <button type="submit">Login</button>
          </form>
      </register-form>
    `);
  });
});
