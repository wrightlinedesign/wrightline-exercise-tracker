import { newE2EPage } from '@stencil/core/testing';

describe('register-form', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<register-form></register-form>');

    const element = await page.find('register-form');
    expect(element).toHaveClass('hydrated');
  });
});
