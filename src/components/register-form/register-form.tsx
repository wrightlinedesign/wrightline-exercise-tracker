import { Component, Host, h, State, Prop } from '@stencil/core';
import { RouterHistory, injectHistory } from '@stencil/router';
import FirebaseAuthService from '../../services/auth.service';
import setAppMessage from '../../utils/setAppMessage';

@Component({
  tag: 'register-form',
  styleUrl: 'register-form.css',
  shadow: false,
})
export class RegisterForm {
  @Prop() history: RouterHistory;
  @State() name: string;
  @State() email: string;
  @State() password: string;

  async handleLogin(event) {
    event.preventDefault();
    const registerRes = await FirebaseAuthService.register({ email: this.email, password: this.password });
    if (registerRes.status == 200) {
      await FirebaseAuthService.updateProfile(this.name);
      // reset inputs
      this.name= '';
      this.email= '';
      this.password = '';

      // move a user on
      this.history.push('/dashboard');
      setAppMessage('success', registerRes.message);
    }
  }

  render() {
    return (
      <Host>
        <h2>Register</h2>
        <form onSubmit={e => this.handleLogin(e)} novalidate>
          <custom-input
            labelText="Display Name"
            type="text"
            name="register-name"
            inputId="register-name"
            value={this.name}
            onInput={(event: any) =>  this.name = event.target.value}
          />
          <custom-input
            labelText="Email"
            type="text"
            name="register-email"
            inputId="register-email"
            value={this.email}
            onInput={(event: any) =>  this.email = event.target.value}
          />
          <custom-input
            labelText="Password"
            type="password"
            name="register-password"
            inputId="register-password"
            value={this.password}
            onInput={(event: any) =>  this.password = event.target.value}
          />
          <button type="submit">Login</button>
        </form>
      </Host>
    );
  }

}
injectHistory(RegisterForm);
