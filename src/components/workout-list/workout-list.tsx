import { Component, Host, h, State } from '@stencil/core';
import { db } from '../../services/firebase';
import state from '../../store';

@Component({
  tag: 'workout-list',
  styleUrl: 'workout-list.css',
  shadow: false,
})
export class WorkoutList {
  @State() workouts: Array<any>;

  componentWillLoad() {
    const workoutRef = db.ref(`workouts/${state.uid}`);
    const workouts = [];
    workoutRef.on('value', (snapshot) => {
      snapshot.forEach((snap) => {
        const id = snap.key;
        const { name, dateAdded } = snap.val();

        workouts.push({id, name, dateAdded});
        this.workouts = workouts;
      })
    });
  }

  render() {
    const placeholders = [];
    for (let i = 0; i < 5; i++) {
      placeholders.push(<skeleton-card>
        <skeleton-text-item width="100"></skeleton-text-item>
        <skeleton-text-item width="80"></skeleton-text-item>
        <skeleton-text-item width="75"></skeleton-text-item>
      </skeleton-card>);
    }
    return (
      <Host>
        <div class="workout-list">
          {(this.workouts) ? this.workouts.map((workout: IWorkout) => (
            <stencil-route-link url={`/workout/${workout.id}`}>
              <div class="workout-item">
                <div class="workout-name">{workout.name}</div>
                <div class="workout-date">{workout.dateAdded}</div>
              </div>
            </stencil-route-link>
          )) :
            placeholders
          }
        </div>
      </Host>
    );
  }

}
