import { newSpecPage } from '@stencil/core/testing';
import { CustomInput } from '../custom-input';

describe('custom-input', () => {
  it('renders text element', async () => {
    const page = await newSpecPage({
      components: [CustomInput],
      html: `<custom-input type="text"></custom-input>`,
    });
    expect(page.root).toEqualHtml(`
      <custom-input class="custom-input" type="text">
        <input class="custom-input__input custom-input__input--text" type="text">
        <span class="custom-input__bar"></span>
        <label class="custom-input__label custom-input__label--text">
          <span class="custom-input__label-text"></span>
        </label>
      </custom-input>
    `);
  });

  it('renders number element', async () => {
    const page = await newSpecPage({
      components: [CustomInput],
      html: `<custom-input type="number"></custom-input>`,
    });
    expect(page.root).toEqualHtml(`
      <custom-input class="custom-input" type="number">
        <input class="custom-input__input custom-input__input--number" type="number">
        <span class="custom-input__bar"></span>
        <label class="custom-input__label custom-input__label--number">
          <span class="custom-input__label-text"></span>
        </label>
      </custom-input>
    `);
  });

  it('renders password element', async () => {
    const page = await newSpecPage({
      components: [CustomInput],
      html: `<custom-input type="password"></custom-input>`,
    });
    expect(page.root).toEqualHtml(`
      <custom-input class="custom-input" type="password">
        <input class="custom-input__input custom-input__input--password" type="password">
        <span class="custom-input__bar"></span>
        <label class="custom-input__label custom-input__label--password">
          <span class="custom-input__label-text"></span>
        </label>
      </custom-input>
    `);
  });

  it('renders radio element', async () => {
    const page = await newSpecPage({
      components: [CustomInput],
      html: `<custom-input type="radio"></custom-input>`,
    });
    expect(page.root).toEqualHtml(`
      <custom-input class="custom-input" type="radio">
        <input class="custom-input__input custom-input__input--radio" type="radio">
        <label class="custom-input__label custom-input__label--radio">
          <span aria-hidden="true" class="custom-input__radio">
            <span class="custom-input__radio-outer"></span>
            <span class="custom-input__radio-inner"></span>
          </span>
          <span class="custom-input__label-text"></span>
        </label>
      </custom-input>
    `);
  });

  it('renders checkbox element', async () => {
    const page = await newSpecPage({
      components: [CustomInput],
      html: `<custom-input type="checkbox"></custom-input>`,
    });
    expect(page.root).toEqualHtml(`
      <custom-input class="custom-input" type="checkbox">
        <input class="custom-input__input custom-input__input--checkbox" type="checkbox">
        <label class="custom-input__label custom-input__label--checkbox">
          <span aria-hidden="true" class="custom-input__checkbox">
            <span class="custom-input__checkbox-outer">
              <svg class="custom-input__tick-icon" viewBox="0 0 20 20" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
                <path class="custom-input__tick" stroke="#000" id="svg_1" d="m1.81495,11.8003l4.67772,5.58065l11.69238,-16.18945" fill-opacity="null" stroke-opacity="null" stroke-width="3" fill="#ffffff"/>
              </svg>
            </span>
          </span>
          <span class="custom-input__label-text"></span>
        </label>
      </custom-input>
    `);
  });
});
