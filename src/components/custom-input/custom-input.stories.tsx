import { action } from '@storybook/addon-actions';
import { convertToAttributes } from '../../utils/storybookUtils';

export default {
  title: 'Components/Custom Input',
  component: 'custom-input',
  argTypes: {
    type: {
      table: {
        disable: true
      }
    },
    handleInput: {
      table: {
        disable: true
      }
    }
  }
}

const Template = (args) => `<custom-input ${convertToAttributes(args).join(' ')} />`;

export const Text = Template.bind({});
Text.args = {
  type: 'text',
  labelText: "Text Input",
  visuallyHidden: false,
  value: '',
  name: "text-input-name",
  inputId: "text-input-id",
  handleInput: action('user Inputted')
}

export const Number = Template.bind({});
Number.argTypes = {
  value: {
    control: {
      type: 'number'
    }
  }
}
Number.args = {
  type: 'number',
  labelText: "Number Input",
  visuallyHidden: false,
  value: '',
  name: "number-input-name",
  inputId: "number-input-id",
  handleInput: action('user Inputted')
}

export const Password = Template.bind({});
Password.args = {
  type: 'password',
  labelText: "Password Input",
  visuallyHidden: false,
  value: '',
  name: "password-input-name",
  inputId: "password-input-id",
  handleInput: action('user Inputted')
}

export const Radio = Template.bind({});
Radio.args = {
  type: 'radio',
  checked: false,
  labelText: "Radio Input",
  visuallyHidden: false,
  name: "radio-input-name",
  inputId: "radio-input-id"
}

export const Checkbox = Template.bind({});
Checkbox.args = {
  type: 'checkbox',
  checked: false,
  labelText: "Checkbox Input",
  visuallyHidden: false,
  name: "checkbox-input-name",
  inputId: "checkbox-input-id"
}
