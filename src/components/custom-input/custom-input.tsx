import { Component, h, Prop, Host } from '@stencil/core';

@Component({
  tag: 'custom-input',
  styleUrl: 'custom-input.css',
  shadow: false,
})
export class CustomInput {
  @Prop() inputId: string;
  @Prop() name!: string;
  @Prop() type!: string;
  @Prop() value: string;
  @Prop() labelText!: string;
  @Prop() visuallyHidden: boolean;
  @Prop() checked: boolean | false;
  @Prop() handleInput: boolean | false;

  isTextInput(type:string) {
    return type === 'text' || type === 'number' || type === 'password';
  }

  renderInput(){
    return(
      <input
        class={(this.value && this.isTextInput(this.type))
          ? `custom-input__input custom-input__input--${this.type} custom-input__input--hasValue`
          : `custom-input__input custom-input__input--${this.type}`}
        type={this.type}
        name={this.name}
        id={(this.inputId) || null}
        checked={this.checked}
        value={this.value || null}
        onInput={((event: any) =>  (this.handleInput) ? this.value = event.target.value : null)}
      />
    );
  }

  renderRadioMarkup() {
    return (
      <span class="custom-input__radio" aria-hidden="true">
        <span class="custom-input__radio-outer"></span>
        <span class="custom-input__radio-inner"></span>
      </span>
    );
  }

  renderCheckboxMarkup() {
    return (
      <span class="custom-input__checkbox" aria-hidden="true">
        <span class="custom-input__checkbox-outer">
          <svg class="custom-input__tick-icon" viewBox="0 0 20 20" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
            <path class="custom-input__tick" stroke="#000" id="svg_1" d="m1.81495,11.8003l4.67772,5.58065l11.69238,-16.18945" fill-opacity="null" stroke-opacity="null" stroke-width="3" fill="#ffffff"/>
          </svg>
        </span>
      </span>
    );
  }

  render() {
    return (
      <Host class="custom-input">
        {this.renderInput()}
        {this.isTextInput(this.type) && <span class="custom-input__bar"></span>}

        <label
          class={`custom-input__label custom-input__label--${this.type}`}
          htmlFor={(this.isTextInput(this.type)) ? this.name : this.inputId}
        >
          {
            this.type === 'radio' && this.renderRadioMarkup()
          }
          {
            this.type === 'checkbox' && this.renderCheckboxMarkup()
          }
          <span class={(this.visuallyHidden) ? `visually-hidden` : `custom-input__label-text`}>
            {this.labelText}
          </span>
        </label>
      </Host>
    );
  }

}
