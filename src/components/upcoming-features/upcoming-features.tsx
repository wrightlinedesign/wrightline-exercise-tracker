import { Component, Host, h } from '@stencil/core';

@Component({
  tag: 'upcoming-features',
  shadow: true,
})
export class UpcomingFeatures {

  render() {
    return (
      <Host>
        <h1>Upcoming Features</h1>
        <h2>For Users</h2>
        <ul>
          <li>
            We are considering reworking exercises to be there own set of data
            <ul>
              <li>This would allow you to select exercises already added</li>
              <li>And would also allow you to easily track progress on an exercise</li>
            </ul>
          </li>
          <li>Global UI improvements</li>
          <li>Adding the ability to remove sets, exercises &amp; workouts</li>
        </ul>
        <h2>For Devs</h2>
        <ul>
          <li>Introduction of StoryBook to project</li>
          <li>
            Move firebase logic into service
            <ul>
              <li>Feed firebase into store rather than directly querying firebase</li>
            </ul>
          </li>
          <li>Consider moving data structure to FireStore</li>
        </ul>
      </Host>
    );
  }

}
