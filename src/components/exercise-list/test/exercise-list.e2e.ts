import { newE2EPage } from '@stencil/core/testing';

describe('exercise-list', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<exercise-list></exercise-list>');

    const element = await page.find('exercise-list');
    expect(element).toHaveClass('hydrated');
  });
});
