import { Component, Host, h, State, Prop } from '@stencil/core';
import { db } from '../../services/firebase';
import state from '../../store';

@Component({
  tag: 'exercise-list',
  styleUrl: 'exercise-list.css',
  shadow: false,
})
export class ExerciseList {
  @Prop() workout: string;
  @State() exercises: Array<any>;

  componentWillLoad() {
    if (!this.workout) {
      return;
    }

    const workoutRef = db.ref(`workouts/${state.uid}/${this.workout}/exercises`);
    this.exercises = [];

    workoutRef.on('child_added', (snapshot) => {
      const exerciseId = snapshot.val();
      if (exerciseId) {
        this.exercises = [...this.exercises, exerciseId];
      }
    });

    workoutRef.on('child_removed', (snapshot) => {
      const removedId = snapshot.val();
      if (removedId) {
        this.exercises = this.exercises.filter((id) => id !== removedId);
      }
    });
  }

  render() {
    return (
      <Host>
        {
          (!this.exercises.length)
            ? <h3>It doesn't look like you've tracked any exercises for this workout!</h3>
            : this.exercises.map((ex) => <exercise-detail exerciseId={ex}></exercise-detail>)
        }
      </Host>
    );
  }

}
