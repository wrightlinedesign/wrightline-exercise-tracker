import { Component, Host, h, Element, Prop } from '@stencil/core';

@Component({
  tag: 'skeleton-text-item',
  styleUrl: 'skeleton-text-item.css',
  shadow: false,
})
export class SkeletonTextItem {
  @Element() el: HTMLElement;

  @Prop() width: string;

  render() {
    if (this.width) {
      this.el.style.setProperty('--placeholder-width', `${this.width}%`);
    }
    return (
      <Host></Host>
    );
  }

}
