import { convertToAttributes } from "../../utils/storybookUtils";

export default {
  title: 'Components/Skeleton Text Item',
  component: 'skeleton-text-item',
  argTypes: {
    width: {
      control: {
        type: 'range',
        min: 0,
        max: 100
      }
    }
  }
}

export const Default = (args:StorybookArgs) => `<skeleton-text-item ${convertToAttributes(args).join(' ')}></skeleton-text-item>`;
Default.args = {
  width: 50
}