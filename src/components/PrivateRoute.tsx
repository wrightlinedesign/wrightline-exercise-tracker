import { FunctionalComponent, h } from '@stencil/core';
import state from '../store';

const PrivateRoute: FunctionalComponent<{url: string, routeRender: Function }> = ({ url, routeRender }) => (
  <stencil-route url={url} routeRender={
    ({ history, match, pages }) => {
      if (state.isAuthenticated) {
        return routeRender({ history, match, pages });
      }
      state.previousUrl = match.url;
      return <stencil-router-redirect url="/" />
    }
  }/>
)

export default PrivateRoute;