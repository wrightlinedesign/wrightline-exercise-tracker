import { newE2EPage } from '@stencil/core/testing';

describe('add-exercise', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<add-exercise></add-exercise>');

    const element = await page.find('add-exercise');
    const customInput = await page.find('custom-input');

    expect(element).toHaveClass('hydrated');
    expect(customInput).toBeTruthy();
  });
});
