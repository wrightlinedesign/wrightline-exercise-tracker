import { newSpecPage } from '@stencil/core/testing';
import { AddExercise } from '../add-exercise';

describe('add-exercise', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [AddExercise],
      html: `<add-exercise></add-exercise>`,
    });
    expect(page.root).toEqualHtml(`
      <add-exercise>
        <form action="">
          <custom-input
            inputid="workout-name"
            labeltext="Exercise Name:"
            name="workout-name"
            type="text">
          </custom-input>
          <wlc-cta type="submit">
            Add
          </wlc-cta>
        </form>
      </add-exercise>
    `);
  });
});
