import { Component, Host, h, Prop, State } from '@stencil/core';
import { db } from '../../services/firebase';
import state from '../../store';
import setAppMessage from '../../utils/setAppMessage';

@Component({
  tag: 'add-exercise',
  styleUrl: 'add-exercise.css',
  shadow: false,
})
export class AddExercise {
  @Prop() workout: string;
  @Prop() exerciseName: string;
  @State() workoutExercises: any;

  async addExercise(event: any) {
    event.preventDefault();

    const exerciseName = this.exerciseName;
    if (!exerciseName) {
      setAppMessage('error', 'Please add a workout name');
      return;
    }

    db.ref(`workouts/${state.uid}/${this.workout}/exercises`).on('value', (snapshot) => {
      this.workoutExercises = snapshot.val();
    })

    const response = await db.ref(`exercises/${state.uid}`).push({
      name: exerciseName
    });
    this.exerciseName = '';

    const exerciseId = response.key;
    if (exerciseId) {
      db.ref(`workouts/${state.uid}/${this.workout}/exercises`).push(exerciseId);
    }
  }

  render() {
    return (
      <Host>
        <form action="" onSubmit={(event) => this.addExercise(event)}>
          <custom-input
            labelText="Exercise Name:"
            type="text"
            name="workout-name"
            inputId="workout-name"
            value={this.exerciseName}
            onInput={(event: any) =>  this.exerciseName = event.target.value}
          />
          <wlc-cta type="submit">Add</wlc-cta>
        </form>
      </Host>
    );
  }

}
