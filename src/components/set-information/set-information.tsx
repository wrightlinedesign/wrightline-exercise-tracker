import { Component, Host, h, Prop, State } from '@stencil/core';
import { db } from '../../services/firebase';
import state from '../../store';

@Component({
  tag: 'set-information',
  styleUrl: 'set-information.css',
  shadow: false,
})
export class SetInformation {
  @Prop() exerciseId: string;
  @State() sets: Array<any>;

  checkForSets() {
    const setRef = db.ref(`exercises/${state.uid}/${this.exerciseId}/sets`);
    this.sets = [];

    setRef.on('child_added', (snapshot) => {
      const exerciseId = snapshot.val();
      if (exerciseId) {
        this.sets = [...this.sets, exerciseId];
      }
    });

    setRef.on('child_removed', (snapshot) => {
      const removedId = snapshot.val();
      if (removedId) {
        this.sets = this.sets.filter((id) => id !== removedId);
      }
    });
  }

  componentWillLoad() {
    this.checkForSets();
  }

  render() {
    return (
      <Host>
        {(this.sets.length) ?
          <ol>
            {this.sets.map((set:ISet) => <li>{set.weight}{set.weightUnit} - {set.reps} reps {set.toFailure && '💪'}</li>)}
          </ol>
        : null}
      </Host>
    );
  }

}
