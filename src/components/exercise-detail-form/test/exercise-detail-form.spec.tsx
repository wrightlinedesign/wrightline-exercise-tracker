import { newSpecPage } from '@stencil/core/testing';
import { ExerciseDetailForm } from '../exercise-detail-form';

describe('exercise-detail-form', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [ExerciseDetailForm],
      html: `<exercise-detail-form></exercise-detail-form>`,
    });
    expect(page.root).toEqualHtml(`
      <exercise-detail-form>
        <button>+ Add a set</button>
        <form action="" class="add-set-form"></form>
      </exercise-detail-form>
    `);
  });
});
