import { newE2EPage } from '@stencil/core/testing';

describe('exercise-detail-form', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<exercise-detail-form></exercise-detail-form>');

    const element = await page.find('exercise-detail-form');
    expect(element).toHaveClass('hydrated');
  });
});
