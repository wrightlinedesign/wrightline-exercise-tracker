import { Component, Host, h, Prop, Element, State } from '@stencil/core';
import { db } from '../../services/firebase';
import state from '../../store';
import merge from 'lodash.merge';

@Component({
  tag: 'exercise-detail-form',
  styleUrl: 'exercise-detail-form.css',
  shadow: false,
})
export class ExerciseDetailForm {
  @Prop() exerciseId: string;
  @State() inputIndex: number = 0;
  @State() formInputs: Array<HTMLCustomInputElement> = [];
  @Element() el: HTMLElement;

  addSetInputs() {
    const inputsToAdd = [
    <div class="exercise-detail-form__input-container">
      <custom-input type="number" name={`set.${this.inputIndex}.reps`} labelText="Reps" handleInput />
      <custom-input type="number" name={`set.${this.inputIndex}.weight`} labelText="Weight" handleInput />
      <custom-input
        name={`set.${this.inputIndex}.weightUnit`}
        inputId={`${this.exerciseId}-weightUnit-radio-0-${this.inputIndex}`}
        type="radio"
        value="kg"
        checked={true}
        labelText="KG"
       />
      <custom-input
        name={`set.${this.inputIndex}.weightUnit`}
        inputId={`${this.exerciseId}-weightUnit-radio-1-${this.inputIndex}`}
        type="radio"
        value="lbs"
        labelText="LBS"
       />
      <custom-input
        name={`set.${this.inputIndex}.toFailure`}
        inputId={`set.${this.inputIndex}.toFailure`}
        type="checkbox"
        labelText="To Failure?"
      />
      <custom-input
        name={`set.${this.inputIndex}.superset`}
        inputId={`set.${this.inputIndex}.superset`}
        type="checkbox"
        labelText="Superset?"
      />
    </div>
    ];
    this.formInputs = [...this.formInputs, ...inputsToAdd];
    this.inputIndex++;
  }

  getInputValue(input:HTMLFormElement) {
    let value:unknown;

    if (input.type === 'checkbox') {
      value = input.checked;
    } else if (input.type === 'radio' && input.checked) {
      value = input.value;
    } else {
      value = input.value;
    }

    return value;
  }

  addSetToFirebase(set:ISet) {
    if (!set) {
      throw Error(`Expected Parameter 1 to be set instead received: ${set}`);
    }
    db.ref(`exercises/${state.uid}/${this.exerciseId}/sets`).push(set);
  }

  addSet(event){
    event.preventDefault();
    const inputs = event.target.getElementsByTagName('input');
    const obj = {};

    [...inputs].forEach((input) => {
      if (input.type === 'radio' && !input.checked) {
        return;
      }

      const keys = input.name.split('.');
      const value = this.getInputValue(input);
      const set = {
        [keys[1]]: {
          [keys[2]]: value
        }
      };

      merge(obj, set);
    });

    Object.values(obj).forEach((set:ISet) => this.addSetToFirebase(set));
    this.formInputs = [];
  }

  render() {
    return (
      <Host>
        <button onClick={() => this.addSetInputs()}>
          {(this.formInputs.length) ? '+ Add another set' : '+ Add a set'}
        </button>
        <form action="" class="add-set-form" onSubmit={(event) => this.addSet(event)}>
          {this.formInputs}
          {(this.formInputs.length) ? <button type="submit">Submit</button> : null}
        </form>
      </Host>
    );
  }

}
