import { newE2EPage } from '@stencil/core/testing';

describe('main-dashboard', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<main-dashboard></main-dashboard>');

    const element = await page.find('main-dashboard');
    expect(element).toHaveClass('hydrated');
  });
});
