import { Component, Host, h, Prop } from '@stencil/core';
import state from '../../store';
import { RouterHistory, injectHistory } from '@stencil/router';

@Component({
  tag: 'main-dashboard',
  styleUrl: 'main-dashboard.css',
  shadow: false,
})
export class MainDashboard {
  @Prop() history: RouterHistory;

  render() {
    return (
      <Host>
        <h2>{ (state.user) ? `Welcome back ${state.user.displayName}!` : 'Welcome!' }</h2>
        <add-workout></add-workout>
        <workout-list>
          <h3>Your workouts</h3>
        </workout-list>
      </Host>
    );
  }

}
injectHistory(MainDashboard);
