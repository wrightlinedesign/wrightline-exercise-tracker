import { Component, Host, h } from '@stencil/core';

@Component({
  tag: 'skeleton-card',
  styleUrl: 'skeleton-card.css',
  shadow: false,
})
export class SkeletonCard {

  render() {
    return (
      <Host>
        <slot></slot>
      </Host>
    );
  }

}
