export default {
  title: 'Components/Skeleton Card',
  component: 'skeleton-card'
}

export const Default = () => `<skeleton-card ></skeleton-card>`;
export const WithTextItems = () => `
<skeleton-card>
  <skeleton-text-item width="80"></skeleton-text-item>
  <skeleton-text-item width="75"></skeleton-text-item>
  <skeleton-text-item width="50"></skeleton-text-item>
  <skeleton-text-item width="60"></skeleton-text-item>
</skeleton-card>
`;
