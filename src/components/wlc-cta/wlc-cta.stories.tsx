import { convertToAttributes } from "../../utils/storybookUtils";

export default {
  title: 'Components/CTA',
  component: 'wlc-cta',
  argTypes: {
    type: {
      control: {
        type: 'select',
        options: ["button", "submit", "internal-link", "external-link"]
      }
    },
    theme: {
      control: {
        type: 'select',
        options: ["primary", "orange", "teal"]
      }
    },
    size: {
      control: {
        type: 'select',
        options: ["primary", "secondary", "tertiary"]
      }
    }
  }
}

const Template = (args:StorybookArgs) => `<wlc-cta ${convertToAttributes(args).join(' ')}>Broken Dream</wlc-cta>`;

export const Primary = Template.bind({});
Primary.args = {
  type: 'button',
  theme: 'primary',
  size: 'primary'
}
Primary.argTypes = {
  size: {
    table: {
      disable: true
    }
  }
}
