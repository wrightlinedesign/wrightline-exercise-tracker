import { newSpecPage } from '@stencil/core/testing';
import { WlcCta } from '../wlc-cta';

describe('wlc-cta', () => {
  it('renders button with type=button', async () => {
    const page = await newSpecPage({
      components: [WlcCta],
      html: `<wlc-cta type="button"></wlc-cta>`,
    });
    expect(page.root).toEqualHtml(`
      <wlc-cta type="button">
        <mock:shadow-root>
          <button type="button" class="cta cta--primary cta--theme-primary">
            <slot></slot>
          </button>
        </mock:shadow-root>
      </wlc-cta>
    `);
  });

  it('renders button with type=submit', async () => {
    const page = await newSpecPage({
      components: [WlcCta],
      html: `<wlc-cta type="submit"></wlc-cta>`,
    });
    expect(page.root).toEqualHtml(`
      <wlc-cta type="submit">
        <mock:shadow-root>
          <button type="submit" class="cta cta--primary cta--theme-primary">
            <slot></slot>
          </button>
        </mock:shadow-root>
      </wlc-cta>
    `);
  });

  it('sets the theme class', async () => {
    const page = await newSpecPage({
      components: [WlcCta],
      html: `<wlc-cta type="button" theme="teal"></wlc-cta>`,
    });
    expect(page.root).toEqualHtml(`
      <wlc-cta type="button" theme="teal">
        <mock:shadow-root>
          <button type="button" class="cta cta--primary cta--theme-teal">
            <slot></slot>
          </button>
        </mock:shadow-root>
      </wlc-cta>
    `);
  });

  it('sets the size class', async () => {
    const page = await newSpecPage({
      components: [WlcCta],
      html: `<wlc-cta type="button" size="secondary"></wlc-cta>`,
    });
    expect(page.root).toEqualHtml(`
      <wlc-cta type="button" size="secondary">
        <mock:shadow-root>
          <button type="button" class="cta cta--secondary cta--theme-primary">
            <slot></slot>
          </button>
        </mock:shadow-root>
      </wlc-cta>
    `);
  });

  it('renders stencil-link', async () => {
    const page = await newSpecPage({
      components: [WlcCta],
      html: `<wlc-cta type="internal-link" href="/"></wlc-cta>`,
    });
    expect(page.root).toEqualHtml(`
      <wlc-cta type="internal-link" href="/">
        <mock:shadow-root>
          <stencil-route-link url="/" class="cta cta--primary cta--theme-primary">
            <div class="cta__external-inner">
              <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-arrow-right fa-w-14 fa-2x"><path fill="currentColor" d="M190.5 66.9l22.2-22.2c9.4-9.4 24.6-9.4 33.9 0L441 239c9.4 9.4 9.4 24.6 0 33.9L246.6 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.2-22.2c-9.5-9.5-9.3-25 .4-34.3L311.4 296H24c-13.3 0-24-10.7-24-24v-32c0-13.3 10.7-24 24-24h287.4L190.9 101.2c-9.8-9.3-10-24.8-.4-34.3z" class=""></path></svg>
              <slot></slot>
            </div>
          </stencil-route-link>
        </mock:shadow-root>
      </wlc-cta>
    `);
  });

  it('renders anchor tag', async () => {
    const page = await newSpecPage({
      components: [WlcCta],
      html: `<wlc-cta type="external-link" href="/"></wlc-cta>`,
    });
    expect(page.root).toEqualHtml(`
      <wlc-cta type="external-link" href="/">
        <mock:shadow-root>
          <a href="/" class="cta cta--primary cta--theme-primary">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-arrow-right fa-w-14 fa-2x"><path fill="currentColor" d="M190.5 66.9l22.2-22.2c9.4-9.4 24.6-9.4 33.9 0L441 239c9.4 9.4 9.4 24.6 0 33.9L246.6 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.2-22.2c-9.5-9.5-9.3-25 .4-34.3L311.4 296H24c-13.3 0-24-10.7-24-24v-32c0-13.3 10.7-24 24-24h287.4L190.9 101.2c-9.8-9.3-10-24.8-.4-34.3z" class=""></path></svg>
            <slot></slot>
          </a>
        </mock:shadow-root>
      </wlc-cta>
    `);
  });
});
