import { Component, Host, h, Prop, Event, EventEmitter, Watch } from '@stencil/core';

@Component({
  tag: 'wl-modal',
  styleUrl: 'wl-modal.css',
  shadow: false,
})
export class WlModal {
  @Prop() visible: Boolean;
  @Prop() shouldOpen: Boolean;

  @Event() wlModalOpened: EventEmitter<Boolean>;
  @Event() wlModalClosed: EventEmitter<Boolean>;

  @Watch('shouldOpen')
  shouldOpenHandler(newValue:Boolean) {
    if (newValue === true) {
      this.openModal();
    } else {
      this.closeModal();
    }
  }

  closeModal() {
    this.visible = false;
    this.wlModalClosed.emit(true);
  }

  openModal() {
    this.visible = true;
    this.wlModalOpened.emit(true);
  }

  render() {
    return (
      <Host class={(this.visible) ? "show" : "hide"}>
        <div class="modal-content">
          <button onClick={() => this.closeModal() } class="modal-close-button">X</button>
          <slot name="modal-title"></slot>
          <slot name="modal-content"></slot>
        </div>
      </Host>
    );
  }

}
