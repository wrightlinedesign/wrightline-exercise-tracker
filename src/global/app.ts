const handleModalOpen = () => {
  const body = document.querySelector('body');
  body.classList.add('modal-open');
  const modalOverlay = document.createElement('div');
  modalOverlay.classList.add('modal-overlay');
  body.appendChild(modalOverlay);
};

const handleModalClose = () => {
  const body = document.querySelector('body');
  body.classList.remove('modal-open');
  const modalOverlay = document.querySelector('.modal-overlay');
  if(modalOverlay){
    modalOverlay.parentElement.removeChild(modalOverlay);
  }
};

export default async () => {
  /**
   * The code to be executed should be placed within a default function that is
   * exported by the global script. Ensure all of the code in the global script
   * is wrapped in the function() that is exported.
   */
  document.addEventListener('wlModalOpened', handleModalOpen);
  document.addEventListener('wlModalClosed', handleModalClose);
};
