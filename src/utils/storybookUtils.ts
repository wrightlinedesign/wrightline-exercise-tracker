export const convertToAttributes = (props: StorybookArgs) => {
  const brokenAttributes = Object.entries(props);
  const attributes = [];
  brokenAttributes.forEach((attr) => {
    const kebabKey = attr[0].replace(/[A-Z]/g, '-$&').toLowerCase();
    const attribute = `${kebabKey}="${attr[1]}"`;
    attributes.push(attribute);
  })

  return attributes;
};
