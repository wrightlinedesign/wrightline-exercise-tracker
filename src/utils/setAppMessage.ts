import state from '../store';

export default (type:string, messageText:string) => {
  state.appMessage = { type, messageText };
}