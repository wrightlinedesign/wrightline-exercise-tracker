import state from '../store';
import { auth } from './firebase';

class FirebaseAuthService {
  constructor() {
    this.init();
  }

  init() {
    try {
      auth.onAuthStateChanged((user => {
        if (user) {
          state.user = user;
          state.uid = user.uid;
          state.isAuthenticated = true;
        } else {
          state.user = undefined;
          state.uid = undefined;
          state.isAuthenticated = false;
        }
      }));
    } catch (error) {
      console.error(error);
    }
  }

  buildResponse (status: Number, data?: any, message?: string, errors?: any){
    return {
      status,
      data,
      errors,
      message
    }
  }

  buildErrorResponse(status: Number, errors: any) {
    return {
      status: status,
      data: null,
      errors,
      message: errors.code
    }
  }

  async getCurrentUser() {
    try {
      const user = await auth.currentUser;
      if (user) {
        // User is signed in.
        if (!state.isAuthenticated) {
          state.isAuthenticated = true
        }
        if (state.user == undefined) {
          state.user = user
        }
        return this.buildResponse(200, user)
      } else {
        state.isAuthenticated = false
        state.user = undefined
      }
      return this.buildResponse(404, {}, 'not found');
    } catch (error) {
      return this.buildErrorResponse(504, error);
    }
  }

  async login(userData) {
    try {
      const userDetails = await auth.signInWithEmailAndPassword(userData.email, userData.password);
      return this.buildResponse(200, userDetails, 'Successfully Logged in');
    } catch (error) {
      return this.buildErrorResponse(504, error);
    }
  }

  /**
  * Handle firebase registration
  * @name register
  *
  * @param {IUserRegister} userData
  *
  * @returns {IBaseRes} Promise that represents <IBaseRes> object
  */
  async register(userData) {
    try {
      const userDetails = await auth.createUserWithEmailAndPassword(userData.email, userData.password);
      return this.buildResponse(200, userDetails, 'Welcome! Thanks for joining us!');
    } catch (error) {
      return this.buildErrorResponse(504, error);
    }
  }

  async logOut() {
    try {
      if (state.isAuthenticated) {
        state.isAuthenticated = false;
        state.user = undefined;
        try {
          await auth.signOut();
        } catch (error) {
          return this.buildErrorResponse(504, error);
        }
      }
      return this.buildResponse(200, {}, 'Logged out successfully');
    } catch (error) {
      return this.buildErrorResponse(504, error);
    }
  }

  async updateProfile(displayName) {
    try {
      let user = await this.getCurrentUser();
      if (user.data) {
        await user.data.updateProfile({
          displayName
        });
        state.user = {
          ...state.user,
          displayName,
        };
      }
      return this.buildResponse(200, {});
    } catch (error) {
      console.log(error);
      return this.buildErrorResponse(504, error);
    }
  }
}

export default new FirebaseAuthService();
