import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const config = {
  apiKey: "AIzaSyCAkZ25pIVwKfs7OLvwORR4EGcc-P5u6cE",
  authDomain: "exercise-tracker-bdd32.firebaseapp.com",
  databaseURL: "https://exercise-tracker-bdd32.firebaseio.com",
  projectId: "exercise-tracker-bdd32",
  storageBucket: "exercise-tracker-bdd32.appspot.com",
  messagingSenderId: "920418804078",
  appId: "1:920418804078:web:1034d94e256a2e1718617f"
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const db = firebase.database();
