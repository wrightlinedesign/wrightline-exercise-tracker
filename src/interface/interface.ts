interface IWorkout {
  id: string,
  name: string,
  dateAdded: string
  exercises?: Array<string>
};

interface ISet {
  reps: number,
  superset?: boolean
  toFailure?: boolean,
  weight: number,
  weightUnit: string,
}

interface IExercise {
  id: string,
  name: string,
  sets?: Array<ISet>
}

type CtaType = "button" | "submit" | "internal-link" | "external-link";
type CtaSize = "primary" | "secondary" | "tertiary";
type CtaTheme = "primary" | "orange" | "teal";

type StorybookArgs = {
  [key: string]: string|boolean|number|Function
}
