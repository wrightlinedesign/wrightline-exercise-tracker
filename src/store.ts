import { createStore } from "@stencil/store";

const { state, onChange } = createStore({
  user: null,
  uid: null,
  isAuthenticated: false,
  appMessage: {
    type: '',
    messageText: ''
  },
  previousUrl: null
});

onChange('user', (value) => {
  state.user = value;
});

onChange('uid', (value) => {
  state.uid = value;
});

onChange('isAuthenticated', (value) => {
  state.isAuthenticated = value;
});

onChange('appMessage', (value) => {
  state.appMessage = value;
});

onChange('previousUrl', (value) => {
  state.previousUrl = value;
})

export default state;